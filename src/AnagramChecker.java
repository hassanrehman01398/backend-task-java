/* Muhammad Hassan Ur Rehman
 * 19th Oct, 2023
 * beyonnex.io | Backend-task
*/

import java.util.*;

public class AnagramChecker {
    private Map<String, List<String>> anagramStoreMap;

    public AnagramChecker() {
        this.anagramStoreMap = new HashMap<>();
    }

    public boolean areAnagrams(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }

        char[] charsWord1 = word1.toLowerCase().toCharArray();
        char[] charsWord2 = word2.toLowerCase().toCharArray();

        Arrays.sort(charsWord1);
        Arrays.sort(charsWord2);

        return Arrays.equals(charsWord1, charsWord2);
    }

    public void addWord(String word) {
        String sortedWord = sortString(word.toLowerCase());

        if (!anagramStoreMap.containsKey(sortedWord)) {
        	anagramStoreMap.put(sortedWord, new ArrayList<>());
        }

        anagramStoreMap.get(sortedWord).add(word);
    }

    public List<String> getAnagrams(String word) {
        String sortedWord = sortString(word.toLowerCase());
        List<String> anagrams = new ArrayList<>(anagramStoreMap.getOrDefault(sortedWord, Collections.emptyList()));
        anagrams.remove(word);
        return anagrams;
    }

    private String sortString(String str) {
        char[] charArray = str.toCharArray();
        Arrays.sort(charArray);
        return new String(charArray);
    }

    public static void main(String[] args) {
        AnagramChecker anagramChecker = new AnagramChecker();

        // Add words for feature #1
        anagramChecker.addWord("listen");
        anagramChecker.addWord("silent");
        anagramChecker.addWord("enlist");
        anagramChecker.addWord("angel");
        anagramChecker.addWord("glean");
        anagramChecker.addWord("acacas");
        anagramChecker.addWord("heart");
        anagramChecker.addWord("earth");

        // Test feature #1
        System.out.println("Are 'listen' and 'enlist' anagrams? " + anagramChecker.areAnagrams("listen", "enlist"));
        System.out.println("Are 'listen' and 'angel' anagrams? " + anagramChecker.areAnagrams("listen", "angel"));
        System.out.println("Are 'heart' and 'earth' anagrams? " + anagramChecker.areAnagrams("heart", "earth"));
        // Test feature #2
        System.out.println("Anagrams of 'listen': " + anagramChecker.getAnagrams("listen"));
        System.out.println("Anagrams of 'angel': " + anagramChecker.getAnagrams("angel"));
        System.out.println("Anagrams of 'heart': " + anagramChecker.getAnagrams("heart"));
        
        
    }
    
}
